## Feature osscan documentation

- [Micronaut Oracle Software Supply Chain Analysis Network documentation](https://micronaut-projects.github.io/micronaut-sql/latest/guide/index.html#_using_osscan)

## Feature http-client documentation

- [Micronaut Micronaut HTTP Client documentation](https://docs.micronaut.io/latest/guide/index.html#httpClient)

